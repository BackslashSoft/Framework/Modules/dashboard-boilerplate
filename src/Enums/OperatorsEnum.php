<?php


namespace Backslash\Dashboard\Boilerplate\Enums;


class OperatorsEnum extends BaseEnum
{
    public const ADDITION = 1;
    public const SUBTRACTION = 2;
    public const MULTIPLICATION = 3;
    public const DIVISION = 4;

    public $Descriptions = [];

    public function __construct()
    {
        $this->Descriptions = [

            self::ADDITION => '+',
            self::SUBTRACTION => '-',
            self::MULTIPLICATION => '*',
            self::DIVISION => '/',
        ];
    }
}