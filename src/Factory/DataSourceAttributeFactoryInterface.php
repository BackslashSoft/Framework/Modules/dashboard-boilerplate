<?php


namespace Backslash\Dashboard\Boilerplate\Factory;

use Backslash\Dashboard\Boilerplate\Model\DataSourceAttributeModel;
use Backslash\Dashboard\Boilerplate\DataSource\DataSourceAttribute;

/**
 * Interface DataSourceAttributeFactoryInterface
 * @package Backslash\Dashboard\Boilerplate\Factory
 */
interface DataSourceAttributeFactoryInterface
{
    /**
     * @param DataSourceAttributeModel $model
     * @return null|DataSourceAttribute
     */
    public function createFromDataSourceAttributeModel(DataSourceAttributeModel $model): ?DataSourceAttribute;
}