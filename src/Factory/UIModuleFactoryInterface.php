<?php

namespace Backslash\Dashboard\Boilerplate\Factory;

use Backslash\Dashboard\Boilerplate\Model\UIModuleModel;
use Backslash\Dashboard\Boilerplate\UIModule\UIModule;

interface UIModuleFactoryInterface
{
    /**
     * @param UIModuleModel $model
     * @return UIModule|null
     */
    public function createFromUIModuleModel(UIModuleModel $model ): ?UIModule;

}