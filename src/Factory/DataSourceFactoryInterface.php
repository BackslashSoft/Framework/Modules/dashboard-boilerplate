<?php

namespace Backslash\Dashboard\Boilerplate\Factory;


use Backslash\Dashboard\Boilerplate\DataSource\DataSource;

/**
 * Interface DataSourceFactoryInterface
 * @package Backslash\Dashboard\Boilerplate\Factory
 */
interface DataSourceFactoryInterface
{
    /**
     * @param int $identifier
     * @return DataSource|null
     */
    public function createDataSource(int $identifier): ?DataSource;

}