<?php

namespace Backslash\Dashboard\Boilerplate\DataSource;


use Backslash\Dashboard\Boilerplate\Model\DataSourceAttributeModel;
use Backslash\Dashboard\Boilerplate\Repository\DataSourceAttributeRepository;
use Spot\Entity;
use Spot\Query;
use View\TwigView;
use ViewModel\MVCViewModel;

/**
 * Class DataSourceAttribute
 * @package Backslash\Dashboard\Boilerplate\DataSource
 * @property DataSourceAttributeModel Model
 */
abstract class DataSourceAttribute
{
    protected $Model;

    /**
     * DataSourceAttribute constructor.
     * @param DataSourceAttributeModel $Model
     */
    public function __construct(DataSourceAttributeModel $Model = null)
    {
        $this->Model = $Model ?: new DataSourceAttributeModel();
    }

    /**
     * @param MVCViewModel $name
     * @return TwigView
     */
    public abstract function render($viewModel);

    /**
     * Function that extracts data and saves defined format in database.
     * @return string|null
     */
    public abstract function processData(): ?string;


    /**
     * @param array $options
     * @return bool
     */
    public function processOptions(array $options): bool
    {
        try {
            foreach ($options['data'] as $name => $value) {
                $this->getChildModel()->$name = $value;
            }
            $this->Model->Active = $options['active'];

        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @param Query $query
     */
    public abstract function prepareQuery(&$query): void;

    public function __get($name)
    {
        return $this->Model->$name;
    }

    public function __set($name, $value)
    {
        $this->Model->$name = $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->Model->Name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->Model->Name = $name;
    }

    /**
     * @return TwigView
     */
    public abstract function renderAttribute();

    /**
     * @param DataSourceAttributeModel $model
     */
    public function setModel(DataSourceAttributeModel $model): void
    {
        $this->Model = $model;
    }

    /**
     * @return DataSourceAttributeModel
     */
    public function getModel(): DataSourceAttributeModel
    {
        return $this->Model;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->Model->Active;
    }

    /**
     * @return bool
     */
    public function isRequired()
    {
        return $this->Model->Required;
    }

    /**
     * @return Entity
     */
    public abstract function getChildModel(): Entity;

    /**
     * @param Entity $dataSourceAttribute
     * @return void
     */
    public abstract function setChildModel($dataSourceAttribute): void;

    /**
     * @param mixed $value
     * @return void
     */
    public abstract function setValue($value): void;


    public abstract function getValue();

    /**
     * @return bool
     */
    public abstract function save(): bool;
    /**
     * @return bool
     */
    public function delete()
    {
        return DataSourceAttributeRepository::getInstance()->delete(['Id' => $this->Model->Id]);
    }
}