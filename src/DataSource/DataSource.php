<?php


namespace Backslash\Dashboard\Boilerplate\DataSource;


use Backslash\Dashboard\Boilerplate\DTO\WidgetDataSourceDTO;
use BoolDataSourceAttribute;
use Backslash\Dashboard\Boilerplate\Widget\Widget;

/**
 * Class DataSource
 * @package Backslash\Dashboard\Boilerplate\DataSource
 * @property int DataSourceIdentifier
 */
abstract class DataSource implements DataSourceInterface
{
    protected $DataSourceIdentifier;

    /**
     * @return array
     */
    public abstract function defaultAttributes(): array;

    /**
     * @param Widget $widget
     * @return WidgetDataSourceDTO
     */
    public abstract function getData(Widget $widget): WidgetDataSourceDTO;


    /**
     * @return int
     */
    public function getDataSourceIdentifier(): int
    {
        return $this->DataSourceIdentifier;
    }

    /**
     * @param int $dataSourceIdentifier
     */
    public function setDataSourceIdentifier(int $dataSourceIdentifier): void
    {
        $this->DataSourceIdentifier = $dataSourceIdentifier;
    }
}