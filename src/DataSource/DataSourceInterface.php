<?php

namespace Backslash\Dashboard\Boilerplate\DataSource;

use Backslash\Dashboard\Boilerplate\DTO\WidgetDataSourceDTO;
use Backslash\Dashboard\Boilerplate\Widget\Widget;

/**
 * Interface DataSourceInterface
 * @package Backslash\Dashboard\Boilerplate\DataSource
 */
interface DataSourceInterface
{
    /**
     * @param Widget $widget
     * @return WidgetDataSourceDTO
     */
    public function getData(Widget $widget): WidgetDataSourceDTO;

    /**
     * @return DataSourceAttribute[]
     */
    public function defaultAttributes(): array;

    /**
     * @return int
     */
    public function getDataSourceIdentifier(): int;

    /**
     * @param int $dataSourceIdentifier
     */
    public function setDataSourceIdentifier(int $dataSourceIdentifier): void;

}