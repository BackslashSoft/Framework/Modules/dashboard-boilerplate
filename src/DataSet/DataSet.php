<?php


namespace Backslash\Dashboard\Boilerplate\DataSet;


use Backslash\Dashboard\Boilerplate\Enums\OperatorsEnum;
use Backslash\Dashboard\Boilerplate\Model\DataSetExpressionDataSourceModel;
use Backslash\Dashboard\Boilerplate\Model\DataSetExpressionModel;
use Backslash\Dashboard\Boilerplate\Model\DataSetModel;

/**
 * Class DataSet
 * @property DataSetModel $Model
 * @package Backslash\Dashboard\Boilerplate\DataSet
 */
class DataSet
{
    protected $Model;

    public function __construct(?DataSetModel $Model = null)
    {
        $this->Model = $Model ?: new DataSetModel();
    }

    public function prepareData()
    {
        $lastOperation = $this->Model->LastDataSetExpression;
        $result = $this->traverse($lastOperation);
    }

    /**
     * @param DataSetExpressionModel $model
     * @return DataSetExpressionDataSourceModel|int|null
     */
    public function traverse(DataSetExpressionModel $model)
    {
        $firstVariableResult = null;
        if ($model->FirstVariable !== null) {
            $firstVariableResult = $this->traverse($model->FirstVariable);
        }
        $secondVariableResult = null;
        if ($model->SecondVariable !== null) {
            $secondVariableResult = $this->traverse($model->SecondVariable);
        }
        if ($model->FirstVariable === null && $model->SecondVariable === null) {
            return $model->DataSetExpressionDataSource;
        }
        switch ($model->Operator->Id) {
            case OperatorsEnum::ADDITION:
                if ($firstVariableResult === null) {
                    $firstVariableResult = 0;
                }
                if ($secondVariableResult === null) {
                    $secondVariableResult = 0;
                }
                $result = $firstVariableResult + $secondVariableResult;
                break;
            case OperatorsEnum::SUBTRACTION:
                $result = $firstVariableResult + $secondVariableResult;
                break;
            case OperatorsEnum::MULTIPLICATION:
                $result = $firstVariableResult + $secondVariableResult;
                break;
            case OperatorsEnum::DIVISION:
                $result = $firstVariableResult + $secondVariableResult;
                break;
            default:
                $result = $firstVariableResult + $secondVariableResult;
                break;

        }
        return $result;

    }
}