<?php

namespace Backslash\Dashboard\Boilerplate\Traits;
use DataSourceEnum;

trait DataSourceTrait
{
    /**
     * @param int $dataSourceId
     * @return null|string
     */
    public
    function returnConfiguredDataSource(int $dataSourceId): ?string
    {
        $dataSourceEnum = new DataSourceEnum();

        if (array_key_exists($dataSourceId, $dataSourceEnum->descriptions)) {
            $dataSourceClassName = $dataSourceEnum->descriptions[$dataSourceId];
            if (class_exists($dataSourceClassName)) {
                return $dataSourceClassName;
            }
        }
        return null;
    }
    public function getDataSourceEnumId($dataSource): int
    {
        $dataSourceEnum = new DataSourceEnum();
        return array_search(get_class($dataSource), $dataSourceEnum->descriptions);
    }

}