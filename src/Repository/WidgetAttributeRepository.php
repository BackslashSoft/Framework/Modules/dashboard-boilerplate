<?php

namespace Backslash\Dashboard\Boilerplate\Repository;

use Backslash\Dashboard\Boilerplate\Model\WidgetAttributeModel;
use Data\Repositories\BaseRepository;

class WidgetAttributeRepository extends BaseRepository
{
    const Model = WidgetAttributeModel::class;
}