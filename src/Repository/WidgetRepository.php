<?php

namespace Backslash\Dashboard\Boilerplate\Repository;

use Backslash\Dashboard\Boilerplate\Model\WidgetModel;
use Data\Repositories\BaseRepository;

class WidgetRepository extends BaseRepository
{
    const Model = WidgetModel::class;
}