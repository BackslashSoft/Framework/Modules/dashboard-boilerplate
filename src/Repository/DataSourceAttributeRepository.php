<?php

namespace Backslash\Dashboard\Boilerplate\Repository;

use Backslash\Dashboard\Boilerplate\Model\DataSourceAttributeModel;
use Data\Repositories\BaseRepository;

class DataSourceAttributeRepository extends BaseRepository
{
    const Model = DataSourceAttributeModel::class;
}