<?php

namespace Backslash\Dashboard\Boilerplate\Repository;

use Backslash\Dashboard\Boilerplate\Model\UIModuleModel;
use Data\Repositories\BaseRepository;

class UIModuleRepository extends BaseRepository
{
    const Model = UIModuleModel::class;
}