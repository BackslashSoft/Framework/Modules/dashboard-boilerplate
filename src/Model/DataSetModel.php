<?php


namespace Backslash\Dashboard\Boilerplate\Model;

use Backslash\Dashboard\Boilerplate\DataSet\DataSet;
use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;


/**
 * Class DataSetModel
 * @package Backslash\Dashboard\Boilerplate\Model
 * @property int Id
 * @property int Order
 * @property int WidgetId
 * @property WidgetModel Widget
 * @property DataSetExpressionModel LastDataSetExpression
 */
class DataSetModel extends Entity
{
    protected static $table = 'data_sets';

    public static function fields()
    {
        $fields = [
            'Id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'Order' => ['type' => 'integer', 'required' => true],
            'WidgetId' => ['type' => 'integer', 'required' => true],
            'DataSetExpressionId' => ['type' => 'integer', 'required' => true]
        ];
        return array_merge($fields, parent::fields());

    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'Widget' => $mapper->belongsTo($entity, WidgetModel::class, 'WidgetId'),
            'LastDataSetExpression' => $mapper->belongsTo($entity, DataSetExpressionModel::class, 'DataSetExpressionId'),
        ];
    }


}