<?php

namespace Backslash\Dashboard\Boilerplate\Model;

use Data\Models\Panel;
use Data\Models\PanelWidget;
use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class WidgetModel
 * @package Backslash\Dashboard\Boilerplate\Model
 * @property int Id
 * @property int UIModuleId
 * @property int DataSourceId
 * @property string Name
 * @property string Description
 * @property UIModuleModel UIModule
 * @property DataSetModel[] DataSet
 * @property Panel[] Panels
 */
class WidgetModel extends Entity
{

    protected static $table = "widgets";

    public static function fields()
    {
        return [
            'Id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'UIModuleId' => ['type' => 'integer'],
            'DataSourceId' => ['type' => 'integer'],
            'Name' => ['type' => 'string'],
            'Description' => ['type' => 'string']
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'UIModule' => $mapper->belongsTo($entity, UIModuleModel::class, 'UIModuleId'),
            'Panels' => $mapper->hasManyThrough($entity, Panel::class, PanelWidget::class, 'PanelId', 'WidgetId'),
            'DataSets' => $mapper->hasMany($entity, DataSetModel::class, 'WidgetId')

        ];
    }
}