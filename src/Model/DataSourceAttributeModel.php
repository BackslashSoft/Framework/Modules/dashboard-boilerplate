<?php

namespace Backslash\Dashboard\Boilerplate\Model;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class DataSourceAttributeModel
 * @package Backslash\Dashboard\Boilerplate\Model
 * @property int Id
 * @property string Name
 * @property string TableProperty
 * @property bool Required
 * @property bool Active
 * @property DataSetExpressionDataSourceModel DataSetExpressionDataSource
 */
class DataSourceAttributeModel extends Entity
{

    protected static $table = "data_source_attributes";

    public static function fields()
    {
        $fields = [
            'Id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'Name' => ["type" => 'string'],
            'TableProperty' => ['type' => 'string'],
            'Required' => ['type' => 'boolean', 'default' => false],
            'Active' => ['type' => 'boolean', 'default' => true],
            'DataSetExpressionDataSourceId' => ['type'=>'integer', 'required' => true]
        ];
        return array_merge($fields, parent::fields());

    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'DataSetExpressionDataSource' => $mapper->belongsTo($entity, DataSetExpressionDataSourceModel::class, 'DataSetExpressionDataSourceId')
        ];

    }


}