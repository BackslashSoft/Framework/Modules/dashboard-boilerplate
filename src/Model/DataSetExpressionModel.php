<?php


namespace Backslash\Dashboard\Boilerplate\Model;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class DataSetExpression
 * @package Data\Models
 * @property int Id
 * @property int FirstVariableId
 * @property int SecondVariableId
 * @property int OperatorId
 * @property int DataSourceId
 * @property boolean Last
 * @property DataSetExpressionModel FirstVariable
 * @property DataSetExpressionModel SecondVariable
 * @property DataSetExpressionDataSourceModel DataSetExpressionDataSource
 * @property DataSetModel DataSet
 * @property OperatorModel Operator
 */
class DataSetExpressionModel extends Entity
{
    protected static $table = 'data_set_expressions';

    public static function fields()
    {
        $fields = [
            'Id' => ['type' => 'integer', 'primary' => true],
            'FirstVariableId' => ['type' => 'integer', 'default' => null],
            'SecondVariableId' => ['type' => 'integer', 'default' => null],
            'OperatorId' => ['type' => 'integer', 'default' => null],
        ];
        return array_merge($fields, parent::fields());
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'FirstVariable' => $mapper->belongsTo($entity, __CLASS__, 'FirstVariableId'),
            'SecondVariable' => $mapper->belongsTo($entity, __CLASS__, 'SecondVariableId'),
            'DataSetExpressionDataSource' => $mapper->hasOne($entity, DataSetExpressionDataSourceModel::class, 'DataSetExpressionDataSourceId'),
            'DataSet' => $mapper->hasOne($entity, DataSetModel::class, 'DataSetExpressionId'),
            'Operator' => $mapper->belongsTo($entity, OperatorModel::class, 'OperatorId'),
        ];
    }

}