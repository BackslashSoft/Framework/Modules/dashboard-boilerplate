<?php


namespace Backslash\Dashboard\Boilerplate\Model;

use Spot\Entity;


/**
 * Class DataSourceModel
 * @package Backslash\Dashboard\Boilerplate\Model
 * @property int Id
 * @property string Caption
 */
class DataSourceModel extends Entity
{
    protected static $table = 'data_sources';

    public static function fields()
    {
        $fields = [
            'Id' => ['type' => 'integer', 'primary' => true],
            'Caption' => ['type'=> 'string']
        ];
        return array_merge($fields, parent::fields());
    }
}