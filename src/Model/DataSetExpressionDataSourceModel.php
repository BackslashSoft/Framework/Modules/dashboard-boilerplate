<?php


namespace Backslash\Dashboard\Boilerplate\Model;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class DataSetDataSourceModel
 * @package Backslash\Dashboard\Boilerplate\Model
 * @property int Id
 * @property int DataSetExpressionId
 * @property int DataSourceId
 * @property DataSetExpressionModel DataSetExpression
 * @property DataSourceModel DataSource
 */
class DataSetExpressionDataSourceModel extends Entity
{
    protected static $table = "data_set_expression_data_sources";

    public static function fields()
    {
        $fields = [
            'Id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'DataSourceId' => ['type' => 'integer', 'required' => true],
            'DataSetExpressionId' => ['type' => 'integer', 'required' => true],
        ];
        return array_merge($fields, parent::fields());

    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'DataSource' => $mapper->belongsTo($entity, DataSourceModel::class, 'DataSourceId'),
            'DataSetExpression' => $mapper->belongsTo($entity, DataSetExpressionModel::class, 'DataSetExpressionId')
        ];

    }
}