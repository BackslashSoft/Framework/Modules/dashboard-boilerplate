<?php

namespace Backslash\Dashboard\Boilerplate\Model;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class UIModuleModel
 * @package Backslash\Dashboard\Boilerplate\Model
 * @property int Id
 * @property WidgetModel Widget
 */
class UIModuleModel extends Entity
{

    protected static $table = "ui_modules";

    public static function fields()
    {
        return [
            "Id" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {

        return [
            'Widget' => $mapper->hasOne($entity, WidgetModel::class, 'WidgetId')

        ];

    }

}