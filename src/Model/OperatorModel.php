<?php


namespace Backslash\Dashboard\Boilerplate\Model;

use Spot\Entity;


/**
 * Class OperatorModel
 * @property int Id
 * @property string Caption
 * @package Backslash\Dashboard\Boilerplate\Model
 */
class OperatorModel extends Entity
{
    protected static $table = 'operators';

    public static function fields()
    {
        $fields = [
            'OperatorId' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'Caption' => ['type' => 'string']
        ];
        return array_merge($fields, parent::fields());

    }
}