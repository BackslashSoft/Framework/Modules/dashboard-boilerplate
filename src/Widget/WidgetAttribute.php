<?php

namespace Backslash\Dashboard\Boilerplate\Widget;


use Backslash\Dashboard\Boilerplate\Model\WidgetAttributeModel;
use Backslash\Dashboard\Boilerplate\Repository\DataSourceAttributeRepository;
use Backslash\Dashboard\Boilerplate\Repository\WidgetAttributeRepository;
use Backslash\Dashboard\Boilerplate\Repository\WidgetRepository;
use Backslash\Dashboard\Boilerplate\Traits\DataSourceTrait;

/**
 * Class WidgetAttribute
 * @package Backslash\Dashboard\Boilerplate\Widget
 * @property WidgetAttributeModel Model
 */
class WidgetAttribute
{
    use DataSourceTrait;

    protected $Model;

    public function __construct(WidgetAttributeModel $model = null)
    {
        $this->Model = $model ?: new WidgetAttributeModel();
    }

    public function __get($name)
    {
        return $this->Model->$name;
    }

    public function __set($name, $value)
    {
        $this->Model->$name = $value;
    }

    /**
     * @param int $widgetId
     * @param int $dataSourceAttributeId
     * @param int|null $widgetAttributeId
     * @return bool
     */
    public function setWidgetAttribute(int $widgetId, int $dataSourceAttributeId, ?int $widgetAttributeId = null): bool
    {
        $existingWidgetAttribute = WidgetAttributeRepository::getInstance()->first(['Id' => $widgetAttributeId, 'DataSourceAttributeId' => $dataSourceAttributeId]);
        if ($existingWidgetAttribute && !$widgetAttributeId) return true;

        if ($widgetAttributeId) {
            $widgetAttributeModel = WidgetAttributeRepository::getInstance()->first(['Id' => $widgetAttributeId]);
            if (!$widgetAttributeModel) {
                return false;
            }
            $this->Model = $widgetAttributeModel;
        }

        $widgetModel = WidgetRepository::getInstance()->first(['Id' => $widgetId]);
        $dataSourceAttributeModel = DataSourceAttributeRepository::getInstance()->first(['Id' => $dataSourceAttributeId]);
        if (!$widgetModel || !$dataSourceAttributeModel) {
            return false;
        }
        $this->Model->WidgetId = $widgetId;
        $this->Model->DataSourceAttributeId = $dataSourceAttributeId;

        return $this->save();
    }

    /**
     * @return WidgetAttributeModel
     */
    public function getWidgetAttribute(): WidgetAttributeModel
    {
        return $this->Model;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        try {
            WidgetAttributeRepository::getInstance()->save($this->Model);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

}