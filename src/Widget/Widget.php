<?php

namespace Backslash\Dashboard\Boilerplate\Widget;


use Backslash\Dashboard\Boilerplate\DataSet\DataSet;
use Backslash\Dashboard\Boilerplate\DataSource\DataSource;
use Backslash\Dashboard\Boilerplate\DataSource\DataSourceAttribute;
use Backslash\Dashboard\Boilerplate\DataSource\DataSourceInterface;
use Backslash\Dashboard\Boilerplate\Model\UIModuleModel;
use Backslash\Dashboard\Boilerplate\Model\WidgetModel;
use Backslash\Dashboard\Boilerplate\Repository\WidgetAttributeRepository;
use Backslash\Dashboard\Boilerplate\Repository\WidgetRepository;
use Backslash\Dashboard\Boilerplate\Traits\DataSourceTrait;
use Backslash\Dashboard\Boilerplate\UIModule\UIModule;
use Data\Dashboard\Factory\DataSourceAttributeFactory;
use Data\Dashboard\Factory\UIModuleFactory;
use ViewModel\Dashboard\WidgetViewModel;
use View\NewDashboard\WidgetView;

/**
 * Class Widget
 * @package Backslash\Dashboard\Boilerplate\Widget
 * @property WidgetModel Model
 * @property DataSource DataSource
 * @property UIModule UIModule
 * @property DataSourceAttribute[] DataSourceAttributes
 * @property DataSet[] DataSets
 */
class Widget
{
    use DataSourceTrait;

    protected $Model;
    protected $DataSource;
    protected $UIModule;
    protected $DataSets;
    protected $DataSourceAttributes;

    /**
     * Widget constructor.
     * @param WidgetModel|null $WidgetModel
     * @param DataSource|null $DataSource
     * @param UIModule|null $UIModule
     * @param DataSet[] $DataSets
     */
    public function __construct(?WidgetModel $WidgetModel = null, ?DataSource $DataSource = null, ?UIModule $UIModule = null, array $DataSets = [])
    {
        $this->Model = $WidgetModel ?: new WidgetModel();
        $this->DataSource = $DataSource;
        $this->UIModule = $UIModule;
        $this->DataSets = $DataSets;
        $this->DataSourceAttributes = $this->initializeAttributes();
    }


    public
    function __get($name)
    {
        return $this->Model->$name;
    }

    public
    function __set($name, $value)
    {
        $this->Model->$name = $value;
    }

    /**
     * @return DataSourceAttribute[]
     */
    public
    function getAttributes(): array
    {
        return $this->DataSourceAttributes;
    }

    /**
     * @return array
     */
    public function initializeAttributes(): array
    {
        $defaultAttributes = $this->DataSource->defaultAttributes();
        if ($this->Model->Id === null) {
            return $defaultAttributes;
        }
        $dataSourceAttributeModels = $this->Model->DataSourceAttributes->entities();
        if (empty($dataSourceAttributeModels)) {
            return $defaultAttributes;
        }
        $dataSourceFactory = new DataSourceAttributeFactory();
        $dataSourceAttributes = [];
        foreach ($dataSourceAttributeModels as $attribute) {
            $attributeInstance = $dataSourceFactory->createFromDataSourceAttributeModel($attribute);
            if ($attributeInstance) {
                $dataSourceAttributes[] = $attributeInstance;
            }
        }
        if (!empty($dataSourceAttributes)) {
            $defaultAttributesNotSaved = array_filter($defaultAttributes,
                function ($defaultAttribute) use ($dataSourceAttributes) {
                    foreach ($dataSourceAttributes as $dataSourceAttribute) {
                        if ($defaultAttribute->getName() === $dataSourceAttribute->getName()) {
                            return false;
                        }
                    }
                    return true;
                });
            $dataSourceAttributes = array_merge($dataSourceAttributes, $defaultAttributesNotSaved);
        }
        return $dataSourceAttributes;
    }

    /**
     *
     * @return array
     */
    public
    function renderAttributes(): array
    {
        $htmlArray = [];
        /**
         * @var DataSourceAttribute $attribute
         */
        foreach ($this->DataSourceAttributes as $attribute) {
            $htmlArray[] = $attribute->renderAttribute();
        }
        return $htmlArray;
    }

    /**
     * @return array|null
     */
    public
    function renderUIModuleOptions(): ?array
    {
        return $this->UIModule->renderOptions();
        if (!$this->UIModule->Model->isNew()) {
            $uiModuleFactory = new UIModuleFactory();
            $UIModuleInstance = $uiModuleFactory->createFromUIModuleModel($this->UIModule->Model);
            if ($UIModuleInstance) {
                return $UIModuleInstance->renderOptions();
            }
        }

        return null;
    }

    /**
     * @return \Backslash\Dashboard\Boilerplate\UIModule\TwigView|null
     */
    public
    function render()
    {
        return $this->UIModule->renderView($this->getDataSource()->getData($this));
    }


    /**
     * @return WidgetView
     */
    public
    function getView(): WidgetView
    {
        $vm = new WidgetViewModel($this);
        return new WidgetView($vm);
    }


    /**
     * @return WidgetModel|null
     */
    public
    function getWidget(): ?WidgetModel
    {
        return $this->Model;
    }

    /**
     * @return DataSourceInterface|null
     */
    public
    function getDataSource(): ?DataSourceInterface
    {
        return $this->DataSource;
    }

    /**
     * @param int $dataSourceId
     * @return bool
     */
    public
    function setDataSource(int $dataSourceId): bool
    {

        $dataSourceClassName = $this->returnConfiguredDataSource($dataSourceId);
        if ($dataSourceClassName) {
            $this->DataSource = new $dataSourceClassName();
            return true;
        }

        return false;
    }

    /**
     * @return UIModule
     */
    public
    function getUIModule()
    {
        return $this->UIModule;
    }

    /**
     * @param UIModuleModel $model
     */
    public
    function setUIModule(UIModuleModel $model): void
    {
        $this->UIModule = $model;
    }

    /**
     * @return bool
     */
    public
    function save(): bool
    {
        try {

            $this->Model->DataSourceId = $this->getDataSourceEnumId($this->DataSource);
            $this->UIModule->save();
            $this->Model->UIModuleId = $this->UIModule->getModel()->Id;
            WidgetRepository::getInstance()->save($this->Model);
            WidgetAttributeRepository::getInstance()->delete(['WidgetId' => $this->getWidget()->Id]);
            foreach ($this->DataSourceAttributes as $attribute) {
                $attribute->save();
                $widgetAttributeModel = new WidgetAttributeModel();
                $widgetAttributeModel->WidgetId = $this->Model->Id;
                $widgetAttributeModel->DataSourceAttributeId = $attribute->getModel()->Id;
                WidgetAttributeRepository::getInstance()->save($widgetAttributeModel);
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param array $dataSourceAttributes
     * @return bool
     */
    private function updateDataSourceOptions(array $dataSourceAttributes): bool
    {
        $success = true;
        foreach ($this->DataSourceAttributes as $attribute) {
            $dataSourceAttributeModel = $attribute->getModel();
            $dataSourceId = $dataSourceAttributeModel->Id;
            if ($dataSourceId) {
                $dataSourceAttributeData = array_filter($dataSourceAttributes, static function ($element) use ($dataSourceId) {
                    return (int)$element['dataSourceAttributeId'] === $dataSourceId;
                });
                if (count($dataSourceAttributeData) > 0) {
                    $firstDataSourceAttributeData = reset($dataSourceAttributeData);

                    $success = $success && $attribute->processOptions($firstDataSourceAttributeData);
                }
            }
        }
        return $success;
    }

    /**
     * @return bool
     */
    public function hasRequiredAttributes(): bool
    {
        $hasRequiredAttributes = array_filter($this->DataSourceAttributes, function ($dataSourceAttribute) {
            return $dataSourceAttribute->isRequired();
        });
        return !empty($hasRequiredAttributes);
    }

    /**
     * @param array $optionData
     * @return bool
     */
    public
    function updateOptions(array $optionData): bool
    {
        $success = true;
        if (array_key_exists('name', $optionData)) {
            $this->setName($optionData['name']);
        }
        if (array_key_exists('description', $optionData)) {
            $this->setDescription($optionData['description']);
        }

        if (array_key_exists('dataSourceAttributes', $optionData)) {
            $success = $this->updateDataSourceOptions($optionData['dataSourceAttributes']);
        }
        if (array_key_exists('uiModuleOptions', $optionData)) {
            $success = $success && $this->UIModule->updateOptions($optionData['uiModuleOptions']);
        }
        return $success;
    }

    /**
     * @return bool
     */
    public
    function delete(): bool
    {
        try {
            $success = true;
            $success = $success && WidgetRepository::getInstance()->delete(['Id' => $this->Model->Id]);
            foreach ($this->getAttributes() as $attribute) {
                $success = $success && $attribute->delete();
            }
            $success = $success && $this->getUIModule()->delete();
        } catch (\Exception $e) {
            return false;
        }
        return $success;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName(string $name): void
    {
        $this->Model->Name = $name;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->Model->Name;
    }

    /**
     * @param string $description
     * @return void
     */
    public function setDescription(string $description): void
    {
        $this->Model->Description = $description;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->Model->Description;
    }


}