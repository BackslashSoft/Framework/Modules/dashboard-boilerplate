<?php

namespace Backslash\Dashboard\Boilerplate\UIModule;


use Backslash\Dashboard\Boilerplate\DTO\WidgetDataSourceDTO;
use Backslash\Dashboard\Boilerplate\Model\UIModuleModel;
use Backslash\Dashboard\Boilerplate\Model\WidgetModel;
use Backslash\Dashboard\Boilerplate\Repository\UIModuleRepository;

/**
 * Class UIModule
 * @package Backslash\Dashboard\Boilerplate\UIModule
 * @property UIModuleModel Model
 */
abstract class UIModule implements UIModuleInterface
{
    protected $Model;


    /**
     * UIModule constructor.
     * @param UIModuleModel|null $model
     */
    public function __construct(UIModuleModel $model = null)
    {
        $this->Model = $model ?: new UIModuleModel();
    }

    /**
     * @param WidgetDataSourceDTO|null $widgetDataSourceDTO
     * @return null|TwigView
     */
    abstract public function renderView(?WidgetDataSourceDTO $widgetDataSourceDTO = null);

    /**
     * @return array|null
     */
    abstract public function renderOptions(): ?array;


    /**
     * @return UIModuleModel
     */
    public function getModel(): UIModuleModel
    {
        return $this->Model;
    }

    public function updateOptions($options) : bool {
        try {
            foreach ($options as $name => $value) {
                $this->getChildModel()->$name = $value;
            }
        }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @param UIModuleModel $model
     */
    public function setModel(UIModuleModel $model): void
    {
        $this->Model = $model;
    }

    /**
     * @return Entity
     */
    public abstract function getChildModel();

    /**
     * @param Entity $childUIModuleEntity
     * @return void
     */
    public abstract function setChildModel($childUIModuleEntity): void;

    /**
     * @return bool
     */
    abstract public function save(): bool;

    /**
     * @return bool
     */
    public function delete() : bool
    {
        return UIModuleRepository::getInstance()->delete(['Id' => $this->Model->Id]) > 0;
    }

}