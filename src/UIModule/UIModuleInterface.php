<?php

namespace Backslash\Dashboard\Boilerplate\UIModule;

use Backslash\Dashboard\Boilerplate\DTO\WidgetDataSourceDTO;

/**
 * Interface UIModuleInterface
 * @package Backslash\Dashboard\Boilerplate\UIModule
 */
interface UIModuleInterface
{
    /**
     * Function that generates html based on data source object.
     * @param WidgetDataSourceDTO|null $widgetDataSourceDTO
     * @return string
     */
    public function renderView(?WidgetDataSourceDTO $widgetDataSourceDTO = null);

    /**
     * Function that generates array of MVCView models.
     * @return null|array
     */
    public function renderOptions(): ?array;

    /**
     * @return bool
     */
    public function save(): bool;

    /**
     * @return bool
     */
    public function delete(): bool;

}