<?php

namespace Backslash\Dashboard\Boilerplate\DTO;

/**
 * Class WidgetDataSourceDTO
 * @package Backslash\Dashboard\Boilerplate\DTO
 * @property array Data
 * @property string Name
 * @property string Description
 */

class WidgetDataSourceDTO
{
    public $Data;
    public $Name;
    public $Description;

    /**
     * WidgetDataSourceDTO constructor.
     * @param array $data
     * @param string|null $name
     * @param string|null $description
     */
    public function __construct(array $data = [], ?string $name = "", ?string $description = "")
    {
        $this->Data = $data;
        $this->Name = $name;
        $this->Description = $description;
    }

}